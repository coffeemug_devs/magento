#!/bin/bash
set -euo pipefail
echo 'create network'

docker network create -d bridge --subnet 192.168.12.0/24 --gateway 192.168.12.1 magentobuild || true

echo 'start redis'
docker stop magentoredis || true
docker container rm magentoredis || true

docker run --name=magentoredis -d \
  --health-cmd='redis-cli ping' \
  --network magentobuild \
  -e ALLOW_EMPTY_PASSWORD=yes \
  bitnami/redis

echo 'start mariadb, import database dump'

docker stop magentomariadb || true
docker container rm magentomariadb || true

docker run --name=magentomariadb -d \
  --network magentobuild \
  --health-cmd='mysqladmin ping --silent' \
  -e ALLOW_EMPTY_PASSWORD=yes \
  --mount=type=bind,source="$(pwd)"/docker/mariadb/docker-entrypoint-initdb.d,target=/docker-entrypoint-initdb.d \
  -e MARIADB_USER=magento_magento \
  -e MARIADB_PASSWORD=8mGib4vFCRoqnaVZYX \
  -e MARIADB_DATABASE=magento_magento \
  bitnami/mariadb:10.1-debian-9

sleep 1

until [ "`docker inspect -f '{{.State.Health.Status}}' magentoredis`" == "healthy" ];
do
  echo "waiting for redis container..."
  sleep 1
done

until [ "`docker inspect -f '{{.State.Health.Status}}' magentomariadb`" == "healthy" ];
do
  echo "waiting for mariadb container..."
  sleep 1
done

echo 'build docker images'

docker build . -t registry.gitlab.com/sstech/magento/app -f Dockerfile --network=magentobuild --target=magento2
docker build . -t registry.gitlab.com/sstech/magento/dev -f Dockerfile --network=magentobuild --target=magento2_dev

echo 'cleanup'

docker stop magentoredis
docker container rm magentoredis
docker stop magentomariadb
docker container rm magentomariadb
docker network rm magentobuild
