#!/bin/bash

file=/opt/bitnami/apache/conf/vhosts/magento-vhosts.conf
if [[ -f "$file" ]]
then
  echo "already setup vhosts"
else
  envsubst < /opt/bitnami/apache/conf/vhosts/magento-vhosts.conf.template > /opt/bitnami/apache/conf/vhosts/magento-vhosts.conf
  envsubst < /opt/bitnami/apache/conf/httpd.conf.template > /opt/bitnami/apache/conf/httpd.conf
fi

DAEMON=httpd
EXEC=/opt/bitnami/apache/bin/httpd

ARGS="-f /opt/bitnami/apache/conf/httpd.conf -D FOREGROUND"

exec ${EXEC} ${ARGS}
