#!/bin/bash

rm -rf /etc/environment
env > /etc/environment
/opt/bitnami/php/bin/php -dmemory_limit=-1 /$WORKDIR/bin/magento cron:install

ln -sf /tmp/stdout  /$WORKDIR/var/log/magento.cron.log

exec cron -f
