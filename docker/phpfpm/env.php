<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'crypt' => [
        'key' => '1945dbbdd19a332c75e7437492b315c4'
    ],
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => getenv('MARIADB_HOST') . ':' . getenv('MARIADB_PORT_NUMBER'),
                'dbname' => getenv('MAGENTO_DATABASE_NAME'),
                'username' => getenv('MAGENTO_DATABASE_USER'),
                'password' => getenv('MAGENTO_DATABASE_PASSWORD'),
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => getenv('MAGENTO_MODE') ? getenv('MAGENTO_MODE') : 'developer',
    'session' => [
        'save' => 'redis',
        'redis' =>
        array (
            'host' => getenv('REDIS_HOST'),
            'port' => getenv('REDIS_PORT'),
            'database' => getenv('REDIS_SESSION_DB') ? getenv('REDIS_SESSION_DB') : 2
        ),
        'disable_locking' => true
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'translate' => 1,
        'config_webservice' => 1,
        'compiled_config' => 1
    ],
    'install' => [
        'date' => 'Thu, 31 May 2018 19:40:49 +0000'
    ],
    'cache' => array(
        'frontend' => array(
            'default' => array(
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => array(
                    'server' => getenv('REDIS_HOST'),
                    'database' => getenv('REDIS_DB') ? getenv('REDIS_DB') : 1,
                    'port' => getenv('REDIS_PORT')
                ),
            )
        )
    ),
];

