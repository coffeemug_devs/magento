#!/bin/bash
set -euxo pipefail

cp /env.php /$WORKDIR/app/etc/env.php

/opt/bitnami/php/bin/php -dmemory_limit=-1 /$WORKDIR/bin/magento setup:upgrade --keep-generated

cd $WORKDIR && chown -R bitnami:daemon generated var

mkfifo $LOG_STREAM || true

chmod 777 $LOG_STREAM

rm -rf $WORKDIR/var/log/*.log
mkdir -p $WORKDIR/var/log/
ln -sfn $LOG_STREAM $WORKDIR/var/log/connector.log
ln -sfn $LOG_STREAM $WORKDIR/var/log/cron.log
ln -sfn $LOG_STREAM $WORKDIR/var/log/adminactivity.log
ln -sfn $LOG_STREAM $WORKDIR/var/log/system.log
ln -sfn $LOG_STREAM $WORKDIR/var/log/debug.log
ln -sfn $LOG_STREAM $WORKDIR/var/log/exception.log

exec php-fpm -F --pid /opt/bitnami/php/tmp/php-fpm.pid -y /opt/bitnami/php/etc/php-fpm.conf | tail -f $LOG_STREAM
